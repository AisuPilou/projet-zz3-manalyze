#include "../src/main.cpp"
#include <iostream>
#include <vector>

#ifndef DEF_PSEUDOCLASS
#define DEF_PSEUDOCLASS

po::variable_value get_value(std::string key, po::variables_map map){
    return map[key];
}

const std::string& as_string(const po::variable_value var){
    return var.as<std::string>();
}

const std::vector<std::string>& as_string_vector(const po::variable_value var){
    return var.as<std::vector<std::string> >();
}

void copy(bfs::path& var1, const bfs::path& var2){
    var1=var2;
}

void copy(bfs::path& var1, const std::string& str){
    var1=str;
}

const std::string& string_path(const bfs::path& path){
    return path.string();
}

void resetRawFormatter(boost::shared_ptr<io::OutputFormatter>& formatter){
    formatter.reset(new io::RawFormatter());
}

void resetJsonFormatter(boost::shared_ptr<io::OutputFormatter>& formatter){
    formatter.reset(new io::JsonFormatter());
}

void set_header(boost::shared_ptr<io::OutputFormatter>& formatter, std::string val){
    formatter->set_header(val);
}

void format(boost::shared_ptr<io::OutputFormatter>& formatter, int val){
    formatter->format(std::cout, val);
}

bool parse_arguments(po::variables_map& vm, std::vector<std::string>& argv){

    char ** argvbis = new char*[argv.size()];

    for(int i = 0; i < argv.size(); i++){
        argvbis[i] = new char[argv[i].length()+1];
        std::strcpy(argvbis[i], argv[i].c_str());
    }

    return parse_args(vm, argv.size(), argvbis);
}

void push_back(std::vector<std::string>& vec, std::string word){
    vec.push_back(word);
}
