#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "pseudoClass.hpp"

using namespace boost::python;

void (*copy1)(bfs::path&, const bfs::path&) = &copy;
void (*copy2)(bfs::path&, const std::string& str) = &copy;

BOOST_PYTHON_MODULE(libapi_manalyze) {
    def("perform_analysis", perform_analysis, args("string, variables_map, string, string_vector, string_vector, config, formatter"));
    def("tokenize_args", tokenize_args);
    def("parse_args", parse_arguments, args("variables_map, string_vector"));  
    def("parse_config", parse_config);
    def("chdir", chdir);
    
    // def("mapGet", mapGet);
    def("push_back", push_back, args("string_vector, string"));
    class_<std::vector<std::string>>("string_vector", init<>())
        .def(vector_indexing_suite<std::vector<std::string>>());

    def("as_string", as_string, args("variable_value"), return_value_policy<copy_const_reference>());
    def("as_string_vector", as_string_vector, args("variable_value"), return_value_policy<copy_const_reference>());
    class_<po::variable_value>("variable_value");

    def("copy", copy1);
    def("copy", copy2);
    def("string_path", string_path, return_value_policy<copy_const_reference>());
    class_<bfs::path>("path")
        .def(init<std::string>())
        .def("parent_path", &bfs::path::parent_path)
        .def("empty", &bfs::path::empty)
        .def(self / std::string());

    def("get_input_files", get_input_files, args("variables_map"));
    class_<std::set<std::string>>("set_string")
        .add_property("range", range(&std::set<std::string>::begin, &std::set<std::string>::end));

    class_<config>("config");

    def("get_value", get_value, args("string, variables_map"));
    class_<po::variables_map>("variables_map")
        .def("count", &po::variables_map::count);

    def("resetRawFormatter", resetRawFormatter, args("formatter"));
    def("resetJsonFormatter", resetJsonFormatter, args("formatter"));
    def("format", format, args("formatter, int"));
    def("set_header", set_header, args("formatter, string"));
    class_<boost::shared_ptr<io::OutputFormatter>>("formatter");

    class_<io::RawFormatter>("RawFormatter");
    class_<io::JsonFormatter>("JsonFormatter");

    class_<yara::Yara>("Yara")
        .def("initialize", &yara::Yara::initialize).staticmethod("initialize");

    class_<plugin::PluginManager, boost::noncopyable>("plugin_manager", no_init)
        .def("get_instance", &plugin::PluginManager::get_instance, return_value_policy<reference_existing_object>())
        .def("load_all", &plugin::PluginManager::load_all)
        .def("unload_all", &plugin::PluginManager::unload_all);

}