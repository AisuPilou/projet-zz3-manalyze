import importlib.util, os
spec = importlib.util.spec_from_file_location("libapi_manalyze", "./bin/libapi_manalyze.so")
manalyze = importlib.util.module_from_spec(spec)
spec.loader.exec_module(manalyze)

if __name__ == "__main__":
    print("""Usage:
  -h [ --help ]         Displays this message.
  -v [ --version ]      Prints the program's version.
  --pe arg              The PE to analyze. Also accepted as a positionalargument. Multiple files may be specified.
  -r [ --recursive ]    Scan all files in a directory (subdirectories will be ignored).
  -o [ --output ] arg   The output format. May be 'raw' (default) or 'json'.
  -d [ --dump ] arg     Dump PE information. Available choices are any combination of: all, summary, dos (dos header),
                        pe (pe header), opt (pe optional header), sections, imports, exports, resources, version,
                        debug, tls, config (image load configuration), delay (delay-load table), rich
  --hashes              Calculate various hashes of the file (may slow down the analysis!)
  -x [ --extract ] arg  Extract the PE resources and authenticode certificates to the target directory.
  -p [ --plugins ] arg  Analyze the binary with additional plugins. (may slow down the analysis!)""")
    cmd = "./bin/manalyze "
    cmd += input("Commande : ")
    argv = manalyze.string_vector()
    
    for opt in cmd.split(' '):
        manalyze.push_back(argv, opt)

    vm = manalyze.variables_map()

    working_dir = manalyze.path("./bin/manalyze")
    working_dir = working_dir.parent_path()
    if working_dir.empty():
	    working_dir = "."
    manalyze.plugin_manager.get_instance().load_all(manalyze.string_path(working_dir))

    conf = manalyze.parse_config(manalyze.string_path(working_dir / "manalyze.conf"))

    if manalyze.parse_args(vm, argv):
        targets = manalyze.get_input_files(vm)

        extraction_directory = ""
        if vm.count("extract"):
            extraction_directory = os.path.abspath(manalyze.as_string(manalyze.get_value("extract", vm)))

        selected_plugins = manalyze.string_vector()
        if vm.count("plugins"):
            selected_plugins = manalyze.tokenize_args(manalyze.as_string_vector(manalyze.get_value("plugins", vm)))

        selected_categories = manalyze.string_vector()
        if vm.count("dump"):
            selected_categories = manalyze.tokenize_args(manalyze.as_string_vector(manalyze.get_value("dump", vm)))

        formatter = manalyze.formatter()
        if (vm.count("output") and manalyze.as_string(manalyze.get_value("output", vm)) == "json"):
            formatter.resetJsonFormatter(formatter)
        else:
            manalyze.resetRawFormatter(formatter)
            manalyze.set_header(formatter, "* Manalyze 0.9 *")

        manalyze.chdir(manalyze.string_path(working_dir))

        count = 0
        for target in targets.range:
            manalyze.perform_analysis(target, vm, extraction_directory, selected_categories, selected_plugins, conf, formatter)
            count += 1
            if count % 1000 == 0:
                manalyze.format(formatter, 0)

        manalyze.format(formatter, 1)

        if vm.count("plugins"):
            manalyze.plugin_manager.get_instance().unload_all()